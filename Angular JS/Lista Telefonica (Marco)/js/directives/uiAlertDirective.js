angular.module("listaTelefonica").directive("uiAlert", function(){
	return {
		templateUrl: "view/alert.html",
		replace: true,
		restrict: "E",
		scope: {
			title: "@",
		},
		transclude: true

		//Tipos de restrição
		//A - Diretiva restrita ao atributo do elemento
		//<div alert></div>

		//E - Diretiva restrita ao elemento
		//<alert></alert>

		//C - Diretiva restrica a classe do elemento
		//<div class="alert"></div>

		//M - Diretiva restrita ao comentário do elemento
		//<-- directive: alert-->
		//<div></div>
	};
});