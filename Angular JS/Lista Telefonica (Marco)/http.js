var router = require('./router');

var app = router(3412);

var operadoras = [
	{

		"nome": "oi",
		"codigo": 14 , 
		"categoria": "Celular", 
		"preco": 2},
	{
		"nome": "vivo", 
		"codigo": 15 , 
		"categoria": "Celular", 
		"preco": 1
	},
	{
		"nome": "tim", 
		"codigo": 41 ,
		"categoria": "Celular", 
		"preco": 3
	},
	{
		"nome": "GVT", 
		"codigo": 25 , 
		"categoria": "Fixo", 
		"preco": 1
	},
	{
		"nome": "Embratel", 
		"codigo": 21 , 
		"categoria": "Fixo", 
		"preco": 2
	}

];

var contatos = [
	{
		"id": 1,
		"nome": "Bruno",
		"telefone": "9999-2222",
		"data": "2015-04-12T12:53:46.204Z",
		"operadora": {
			"nome": "Oi",
			"codigo": 14,
			"categoria": "Celular"
		}
	},
	{
		"id": 2,
		"nome": "Sandra",
		"telefone": "9999-3333",
		"data": "2015-04-12T12:53:47.204Z",
		"operadora": {
			"nome": "Vivo",
			"codigo": 15,
			"categoria": "Celular"
		}
	},
	{
		"id": 3,
		"nome": "Mariana",
		"telefone": "9999-4444",
		"data": "2015-04-12T12:53:48.204Z",
		"operadora": {
			"nome": "Tim",
			"codigo": 41,
			"categoria": "Celular"
		}
	}
];

app.interceptor(function(req, res, next){
	console.log("1");
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Headers', 'Content-Type')
	next();
});

app.interceptor(function(req, res, next){
		console.log("2");

	res.setHeader('Content-Type', 'application/json;charset=UTF-8');
	next();
});

app.get('/operadoras', function (req, res){
	res.write(JSON.stringify(operadoras));
	res.end();

});

app.get('/contatos', function (req, res){
	res.write(JSON.stringify(contatos));
	res.end();

});

app.post('/contatos', function (req, res){
	var contato = req.body;
	contatos.push(JSON.parse(contato));

	res.end();
});

app.options('/contatos', function(req, res){
	res.end();
	
});

console.log("Running");