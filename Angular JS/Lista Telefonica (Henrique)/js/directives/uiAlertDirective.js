angular.module("listaTelefonica").directive("uiAlert", function(){
    return{
        templateUrl: "view/alert.html",
        replace: true, //substitiu na chamada
        restrict: "AE", // restinge o modo de utilizacao A atrubuto E elemento C classe M comentario
        scope:{ //compsrtilhs o scopo de onde ela eh utilizada
            title:"@title"
            // title:"@" mesmo
            // message:"=" leva o conteudo do escopo //valor da variavel
        },
        transclude: true //
    };
});