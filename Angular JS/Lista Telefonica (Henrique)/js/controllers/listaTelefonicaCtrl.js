angular.module("listaTelefonica").controller("listaTelefonicaCtrl", function ($scope, contatosAPI, operadorasAPI,serialGenerator) {
    $scope.app = "Lista Telefonica";
    $scope.contatos = [];
    $scope.operadoras = [];
    var carregarContatos = function () {
        contatosAPI.getContatos().then(
            function (response) {
                $scope.contatos = response.data;
            },
            function (error) {
                $scope.message = "Deu doidera: " + error;
            }
        );

    }; 
    $scope.adicionarContato = function (contato){
        contato.serial = serialGenerator.generate();
        contato.data = new Date();
        contatosAPI.saveContato(contato).then(
            function (response) {
                delete $scope.contato;
                $scope.contatoForm.$setPristine();
                carregarContatos();
            },
            function (error) {
            // console.log(error);
             	$scope.message = "Deu doidera: " + error;
            }
        );
    };
    var carregarOperadoras = function () {
        operadorasAPI.getOperadoras().then(
            function (response) {
                $scope.operadoras = response.data;
            },
            function (error) {
            	$scope.message = "Aconteceu um problema: " + error;
            }
        );
    };
   
    $scope.apagarContatos = function(contatos){
        $scope.contatos = contatos.filter(function (contato){
            if(!contato.selecionado) return contato;
        });
        console.log(contatosSelecionados);
    };
    $scope.isContatoSelecionado = function (contatos){
        return contatos.some( function (contato) {
            return contato.selecionado;
        });
    }
    $scope.ordenarPor = function(campo){
        $scope.criterioDeOrdenacao = campo;
        $scope.direcaoDeOrdenacao = !$scope.direcaoDeOrdenacao;
    };
    carregarContatos();
    carregarOperadoras();
});