angular.module("listaTelefonica").filter("name", function (){
    return function(input){
        var listaDeNomes = input.split(" ");
        var listaDeNomesFormatada = listaDeNomes.map(function(nome){
            if(nome.length > 2)
            // if(/(da|de)/.test(nome))
            return nome.charAt(0).toUpperCase() + nome.substring(1).toLowerCase();
        });
        // console.log(listaDeNomesFormatada);
        return listaDeNomesFormatada.join(" ");
    };
});