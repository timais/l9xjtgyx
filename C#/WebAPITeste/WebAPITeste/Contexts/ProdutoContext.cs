﻿using LinqToDB.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPITeste.Contexts
{
    public class ProdutoContext : DataConnection
    {
        public ProdutoContext() : base(Constants.produtos) { }
    }
}