﻿namespace WebAPITeste.Models
{
    public class Produto
    {

        [Column("id_produto"), PrimaryKey, Identity]
        public int IdProduto { get; set; }
        [Column(Name = "nome", Length = 45)]
        public string Nome { get; set; }
        [Column(Name = "preco")]
        public decimal Preco { get; set; }
        [Column(Name = "categoria", Length = 45)]
        public string Categoria { get; set; }
        [Column(Name = "marca", Length = 45)]
        public string Marca { get; set; }

        //[Association(ThisKey = nameof(IdList), OtherKey = nameof(TodoItem.IdList))]
        //public IEnumerable<TodoItem> Itens { get; set; }
    }
}