﻿using LinqToDB;
using LinqToDB.Data;
using Newtonsoft.Json;
using ORM.Contexts;
using ORM.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ORM
{
    class Program
    {
        static void Main(string[] args)
        {
            ConectarAoBancoDeDados();
        }

        private static void ConectarAoBancoDeDados()
        {
            using (var db = new TreinamentoContext())
            {

                //db.CreateTable<TodoList>();
               
                var json = global::ORM.Properties.Resources.json;

                var listaDeLista = JsonConvert.DeserializeObject<List<TodoList>>(json);
                db.BulkCopy(listaDeLista);
                /// CRUD (C)
                /// 
                /// var versao = db.Execute<string>("SELECT @@version;");
                /// Console.WriteLine(versao);
                ///  
                ///var lista = new TodoList();
                ///lista.IdList = 1;
                ///lista.Nome = "Tarefas de FDS";
                ///lista.Usuario = "eu";
                ///  
                ///var lista = new TodoList();
                ///lista.IdList = 2;
                ///lista.Nome = "Trabalhos de Faculdade";
                ///lista.Usuario = "eu";
                ///  
                ///var lista = new TodoList();
                ///lista.IdList = 3;
                ///lista.Nome = "Soninho da madrugada";
                ///lista.Usuario = "eu";
                ///
                ///foreach (var lista in listaDeLista)
                ///{
                ///    lista.IdList++;
                ///    try
                ///    {
                ///        db.Insert(lista);
                ///    }
                ///    catch
                ///    {
                ///        Console.ForegroundColor = ConsoleColor.Yellow;
                ///        Console.WriteLine("Valor ja existente na PK, atualizando.");
                ///        db.Update(lista);
                ///        Console.ForegroundColor = ConsoleColor.White;
                ///    }
                ///}
                ///
                //foreach (var lista in listaDeLista)
                //{
                //    lista.IdList ++;
                //    try
                //    {
                //        db.InsertOrReplace(lista);
                //    }
                //    catch (Exception ex)
                //    {
                //        Console.ForegroundColor = ConsoleColor.Yellow;
                //        Console.WriteLine($"Erro ao salvar {ex.ToString()}");
                //        db.Update(lista);
                //        Console.ForegroundColor = ConsoleColor.White;
                //    }
                //}
                //var consulta = db.GetTable<TodoList>().Where(l => l.Nome.Contains("Lorem"));

                //var consulta = db.GetTable<TodoList>();
                //var count = consulta.Count();
                //var listaLorem = consulta.Skip(10).Take(10).Sum(l => l.Nome.Length);

                //var roberson = new TodoList {
                //    IdList = 159
                //};

                //db.Delete(roberson);

                //var consulta = from l in db.GetTable<TodoList>()
                //               where l.Nome.Contains("ipsum")
                //               select l;

                //consulta.Set(t => t.Usuario, "feijoada").Set(t => t.Nome, "nada acontece").Update();

                
                
            }

            Console.WriteLine("Digite uma tecla para sair.");
            Console.Read();
        }
    }
}
