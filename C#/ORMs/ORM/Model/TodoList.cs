﻿using LinqToDB.Mapping;

namespace ORM.Model
{
    [Table(Name = "todo_list")]
    public class TodoList
    {
        [Column("id_list"), PrimaryKey, Identity]
        public int IdList { get; set; }

        [Column(Name = "nome", Length = 50)]
        public string Nome { get; set; }

        [Column("usuario", Length = 50)]
        public string Usuario { get; set; }
    }
}
