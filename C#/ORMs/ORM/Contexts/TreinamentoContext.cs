﻿using LinqToDB.Data;
using ORM.Model;

namespace ORM.Contexts
{
    public class TreinamentoContext : DataConnection
    {
        public TreinamentoContext() : base(Constants.Treinamento) { }
    }
}
