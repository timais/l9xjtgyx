﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2
{

    public static class MyLinqImplementation
    {
        public static IEnumerable<T> 
            Where<T>(this IEnumerable<T> source,
            Func<T, bool> predicate)
        {
            foreach (T item in source)
                if(predicate(item))
                    yield return item;
                
        }
        public static IEnumerable<TResult>
            Select<TSource, TResult>(this IEnumerable<TSource> source,
            Func<TSource, TResult> selector)
        {
            foreach(TSource item in source)
            {
                yield return selector(item); 
            }
        }
    }
    class Program
    {
       
        static void Main(string[] args)
        {

            //var sequence = GenerateSequence();
            var sequence = GenerateNumbers()
                .Select((n, index) =>
                new
                {
                    index,
                    formattedResult = n.ToString().PadLeft(20)
                });
           

            foreach (var item in sequence)
            {
                Console.WriteLine(item);
            }

            var sequence2 = GenerateNumbers().Where(n => n % 3 == 0);
            foreach (var item in sequence2)
            {
                Console.WriteLine(item);
            }

        }
        private static IEnumerable<string> GenerateSequence()
        {
            var i = 0;
            while (i++ < 100) yield return i.ToString();

        }
        private static IEnumerable<int> GenerateNumbers()
        {
            var i = 0;
            while (i++ < 100) yield return i;

        }
    }
}
