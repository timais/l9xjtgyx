﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            var today = DateTime.Now;

            var sequence = GeneratedStrings(() => 
            {
                today += TimeSpan.FromDays(7);
                return today;
            });


            foreach (var item in sequence.Take(100))
            {
                Console.WriteLine(item); 
            }
        }
        private static IEnumerable<string> GeneratedStrings()
        {
            var rval = new List<string>();
            int i = 0;
            while(i++ < int.MaxValue)
            {
                yield return i.ToString();
            }
        }
        private static IEnumerable<T> GeneratedStrings<T>(Func<T> itemGenerator)
        {
            var rval = new List<T>();
            int i = 0;
            while (i++ < 100)
            {
                //yield return itemGenerator();
                rval.Add(itemGenerator());
            }
                return rval;
        }
    }
}
