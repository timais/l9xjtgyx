﻿using System;
using System.Timers;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Timer myTimer = new Timer(100);
            myTimer.Elapsed += MyTimer_Elapsed;
            myTimer.Start();
            Console.ReadKey();

        }

        private static void MyTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("MuDaNdO aS cOrEs MuDaNdO aS cOrEs MuDaNdO aS cOrEs MuDaNdO aS cOrEs MuDaNdO aS cOrEs");

            Random random = new Random();
            var cor1 = (ConsoleColor)random.Next(1, 14);
            var cor2 = (ConsoleColor)random.Next(1, 14);

            Console.ForegroundColor = cor1;
            Console.BackgroundColor = cor2;

        }
    }
}
