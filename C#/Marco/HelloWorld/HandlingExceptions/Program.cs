﻿using System;
using System.IO;

namespace HandlingExceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
            string content = File.ReadAllText(@"C:\Users\marco\Desktop\fil.txt");
            Console.WriteLine(content);
                
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine("There was a problem!");
                Console.WriteLine("Make sure the name of the file is correctely");
            }
            catch(DirectoryNotFoundException ex)
            {
                Console.WriteLine("There was a problem!");
                Console.WriteLine("Make sure the directory exists");
            }
            catch(Exception e){
                Console.WriteLine("There was a problem");
                Console.WriteLine(e.Message);
            }
            finally
            {
                //code to finalize
                //settings objects to null
                //closing database connections
                Console.WriteLine("Closing aplication now");
            }
        }
    }
}
