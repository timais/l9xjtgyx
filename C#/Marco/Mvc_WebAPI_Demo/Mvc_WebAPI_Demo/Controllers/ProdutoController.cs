﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Mvc_WebAPI_Demo.Models;

namespace Mvc_WebAPI_Demo.Controllers
{
    public class ProdutoController : ApiController
    {
        Produto[] produtos = new Produto[]
        {
            new Produto {Id = 1, Nome = "Nokia Lumia", Categoria = "Celulares", Preco = 899.00M},
            new Produto {Id = 2, Nome = "NoteBook", Categoria = "Informatica", Preco = 1299.50M},
            new Produto {Id = 3, Nome = "Monitor 25", Categoria = "Informatica", Preco = 489.75M},
            new Produto {Id = 4, Nome = "Mouse", Categoria = "Acessorios", Preco = 3.75M},
            new Produto {Id = 5, Nome = "Teclado", Categoria = "Acessorios", Preco = 16.99M}
        };
        
        public IEnumerable<Produto> GetTodosProdutos()
        {
            return produtos;
        }

        public IHttpActionResult GetProduto(int id)
        {
            var produto = produtos.FirstOrDefault(p => p.Id == id);
            if (produto == null)
            {
                return NotFound();
            }
            return Ok(produto);
        }
        public IEnumerable<Produto> GetProdutoPorCategoria(string cat)
        {
            return produtos.Where(p => string.Equals(p.Categoria, cat, StringComparison.OrdinalIgnoreCase));
        }
    }
}
