﻿app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/listas', {
            templateUrl: '/Content/Views/listas.html',
            controller: 'listasController'
        })
        .when('/', {
            templateUrl: '/Content/Views/home.html'
        })
        .when('/lista/:id', {
            templateUrl: '/Content/Views/lista.html',
            controller: 'listaController'
    });

    $locationProvider.html5Mode(true);
});