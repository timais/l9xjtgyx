﻿app.service("CommonService", function ($resource) {
    return {
        TodoList: $resource('/api/todo/:id', { id: '@id' }, {
            salvar: { url: '/api/todo/:id', method: 'PUT' },
            'put': { method: 'PUT' }
        })
    };
});