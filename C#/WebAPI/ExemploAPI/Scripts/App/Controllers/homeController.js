﻿app.controller("homeController", function ($scope, $mdDialog) {
    $scope.Text = "<h1>Nao sei o que to fazendo</h1>"

    $scope.fodase = function (ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Nada acontece')
                .textContent('Feijoada')
                .ariaLabel('Nada acontece')
                .ok('Got it!')
                .targetEvent(ev)
        );
    }
});