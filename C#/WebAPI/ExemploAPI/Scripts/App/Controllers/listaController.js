﻿app.controller("listaController", function ($scope, CommonService, $routeParams, $rootScope) {
    var id = 0;

    $scope.Init = function () {
        id = $routeParams.id;
        $scope.Lista = { Nome: id };

        CommonService.TodoList.get({ id: id }).$promise.then(function (d) {
            $scope.Lista = d;
        })
    };

    $scope.Salvar = function () {
        CommonService.TodoList.put($scope.Lista).$promise.then(function (d) {
            history.back(1); 
        })
    };
});