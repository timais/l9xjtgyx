﻿app.controller("listasController", function ($scope, $http, CommonService, $rootScope) {
    $scope.query = {
        Page: 1,
        Limit: 12,
        Order: "IdList"
    }

    $scope.setFeijoada = function (list) {
        list.Usuario = "Feijoada!!";
        CommonService.TodoList.salvar(list).$promise.then(function (d) {
            $scope.Init();
        });
    }

    $scope.excluirLista = function (list) {
        list.Usuario = "Feijoada!!";
        CommonService.TodoList.delete({ id: list.IdList }).$promise.then(function (d) {
            $scope.Init();
        });
    }


    $scope.Editar = function (list) {
        $rootScope.Ir("/lista/" + list.IdList);
    }

    $scope.Init = function () {
        CommonService.TodoList.get($scope.query).$promise.then(function (d) {
            $scope.Listas = d.Response;
            $scope.Total = d.Count
        });

    };
})