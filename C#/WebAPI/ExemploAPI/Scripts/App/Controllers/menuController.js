﻿app.controller("menuController", function ($scope, $rootScope, $mdSidenav, $http, $location) {
      
    $scope.fecharMenu = function () {
        $mdSidenav('left').close()
    }

    $rootScope.Ir = function (url) {        
        $location.path(url);
    }    
});