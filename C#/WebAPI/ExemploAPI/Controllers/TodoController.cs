﻿using ExemploAPI.Business;
using ExemploAPI.Helpers;
using ExemploAPI.Models;
using ExemploAPI.Models.ViewModel;
using System.Collections.Generic;
using System.Web.Http;

namespace ExemploAPI.Controllers
{
    public class TodoController : ApiController
    {
        TodoBusiness business;

        public TodoController()
        {
            this.business = new TodoBusiness();
        }

        public Pack<IEnumerable<TodoList>> Get([FromUri]Paginacao p)
        {
            return Pack<IEnumerable<TodoList>>.Paginate(business.ObterListas(), p);
        }

        public TodoList Get(int id)
        {
            return business.ObterLista(id);
        }

        public int Post(TodoList list)
        {
            return business.IncluirLista(list);
        }

        public int Put([FromBody]TodoList list)
        {
            return business.AtualizarLista(list);
        }

        public int Delete(int id)
        {
            return business.ExcluirLista(id);
        }
    }
}