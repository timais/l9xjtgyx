﻿using System.Threading.Tasks;
using System.Web.Http;

namespace ExemploAPI.Controllers
{
    public class FeijoadaController : ApiController
    {
        public async Task<string> GetFeijoada(int period)
        {
            await Task.Delay(period);
            return $"Nada acontece por {period} milisegundos";
        }
    }
}