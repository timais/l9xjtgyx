﻿namespace ExemploAPI.Models.ViewModel
{
    public class Paginacao
    {
        public int Limit { get; set; }
        public int Page { get; set; }
        public string Order { get; set; }
    }
}