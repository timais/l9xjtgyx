﻿using LinqToDB.Mapping;
using System.Collections.Generic;

namespace ExemploAPI.Models
{

    [Table(Name = "todo_list")]
    public class TodoList
    {
        [Column("id_list"), PrimaryKey, Identity]
        public int IdList { get; set; }

        [Column(Name = "nome", Length = 50)]
        public string Nome { get; set; }

        [Column("usuario", Length = 50)]
        public string Usuario { get; set; }

        [Association(ThisKey = nameof(IdList), OtherKey = nameof(TodoItem.IdList))]
        public IEnumerable<TodoItem> Itens { get; set; }
    }
}