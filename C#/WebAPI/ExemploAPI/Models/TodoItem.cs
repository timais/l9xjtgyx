﻿using LinqToDB.Mapping;

namespace ExemploAPI.Models
{

    [Table(Name = "todo_item")]
    public class TodoItem
    {
        [Column("id_list"), PrimaryKey]
        public int IdList { get; set; }

        [Column("id_item"), PrimaryKey, Identity]
        public int IdItem { get; set; }

        [Column(Name = "text", Length = 50)]
        public string Text { get; set; }

        [Column("ies_ok")]
        public bool IesOk { get; set; }
    }
}