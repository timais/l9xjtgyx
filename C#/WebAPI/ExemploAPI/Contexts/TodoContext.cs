﻿using ExemploAPI.Models;
using LinqToDB;
using LinqToDB.Data;

namespace ExemploAPI.Contexts
{
    public class TodoContext : DataConnection
    {
        public TodoContext() : base(Constants.Treinamento)
        {
            LinqToDB.Common.Configuration.Linq.AllowMultipleQuery = true;
        }

        public ITable<TodoList> TodoLists => GetTable<TodoList>();
        public ITable<TodoItem> TodoItems => GetTable<TodoItem>();
    }
}