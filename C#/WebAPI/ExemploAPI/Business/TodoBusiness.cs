﻿using ExemploAPI.Contexts;
using ExemploAPI.Models;
using ExemploAPI.Models.ViewModel;
using LinqToDB;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;

namespace ExemploAPI.Business
{
    public class TodoBusiness
    {
        public TodoList ObterLista(int id)
        {
            using (var db = new TodoContext())
            {
                return db.GetTable<TodoList>()
                    .LoadWith(t => t.Itens)
                    .Where(l => l.IdList == id).FirstOrDefault();
            }
        }

        public int AtualizarLista(TodoList list)
        {
            using (var db = new TodoContext())
            {
                return db.Update(list);
            }
        }

        public int ExcluirLista(int id)
        {
            using (var db = new TodoContext())
            {
                return db.GetTable<TodoList>().Where(l => l.IdList == id).Delete();
            }
        }

        public int IncluirLista(TodoList list)
        {
            using (var db = new TodoContext())
            {
                return db.InsertWithInt32Identity(list);
            }
        }

        public IQueryable<TodoList> ObterListas()
        {
            var db = new TodoContext();
            return db.GetTable<TodoList>();
        }
    }
}