﻿using System.Web.Mvc;
using System.Web.Routing;

namespace ExemploAPI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "mvc/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               "Root",
               "{*pathInfo}",
               new { controller = "Home", action = "Index", id = UrlParameter.Optional });
        }
    }
}
