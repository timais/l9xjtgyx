﻿using ExemploAPI.Models.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;

namespace ExemploAPI.Helpers
{
    public class Pack<T>
    {
        public object Response { get; }
        public Pack(T response)
        {
            this.Response = response;
        }
        

        public static Pack<IEnumerable<T>> Paginate<T>(IQueryable<T> list, Paginacao pag) where T : class
        {
            if (pag == null)
                pag = new Paginacao();

            if (pag.Limit == 0)
                pag.Limit = 20;

            if (pag.Page == 0)
                pag.Page = 1;

            if (!string.IsNullOrEmpty(pag.Order))
            {
                if (pag.Order.StartsWith("-"))
                    list = list.OrderBy($"{pag.Order.Remove(0, 1)} descending");
                else
                    list = list.OrderBy(pag.Order);
            }
            var paginatedList = list.Skip(pag.Limit * (pag.Page - 1)).Take(pag.Limit);
            Pack<IEnumerable<T>> p = new Pack<IEnumerable<T>>(paginatedList);
            p.Count = list.Count();
            return p;
        }

        private int? _count;

        public int? Count
        {
            get
            {
                if (_count != null)
                    return _count;

                if (Response != null)
                {
                    if (Response.GetType().GetInterfaces().Any(
                       i => i.IsGenericType &&
                       i.GetGenericTypeDefinition() == typeof(IEnumerable<>)))
                        return (from object Item in (Response as IEnumerable)
                                select Item).Count();

                }
                return null;
            }

            set
            {
                _count = value;
            }
        }
    }
}